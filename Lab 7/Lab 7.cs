﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_7
{
    class Program
    {
        static void Main(string[] args)
        {
            double b, c, x, a, h;
            Console.Write("x = ");
            x = Convert.ToDouble(Console.ReadLine());
            a = A(x);
            b = B(x);
            c = C(x);
            h = H(a, b, c);
            Console.Write("\nA = " + a);
            Console.Write("\nB = " + b);
            Console.Write("\nC = " + c);
            Console.Write("\nH = " + h);
            Console.ReadKey();
        }

        static public double H(double a,double b, double c)
        {
            double y;
            y = Math.Pow(a, 2)+ Math.Pow(b, 2) - 6*c;
            return y;
        }
        static public double A(double x)
        {
            double y;
            y = Math.Pow(x, 2) + Math.Exp(-x);
            return y;
        }
        static public double B(double x)
        {
            double y;
            y = Math.Log(x) + Math.Sqrt(x);
            return y;
        }
        static public double C(double x)
        {
            double y;
            y = Math.Pow(Math.Cos(x), 2) + Math.Pow(x, 5);
            return y;
        }
    }
}
