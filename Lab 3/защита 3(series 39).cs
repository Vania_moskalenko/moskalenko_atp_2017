﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication16
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите число K");
            int K = Convert.ToInt32(Console.ReadLine());
            int k = 0;
            int a = 0;
            int b = 0;
            int c = 0;
            for (int i = 0;i<K;i++)
            {
                Console.WriteLine($"Введите {i+1} набор чисел");
                a =Convert.ToInt32(Console.ReadLine());
                b = Convert.ToInt32(Console.ReadLine());
                c = Convert.ToInt32(Console.ReadLine());

                if (b < a && b < c)
                    k = k + 1;
                else if(b>a&&b>c)
                        k = k + 1;
            }
            Console.WriteLine($"Количество наборов = {k}");
        }
    }
}
