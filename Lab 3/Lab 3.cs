﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_3
{
    class Program
    {
        static int math(int z, double x, double y)
        {
             if(z<0)
               {
                x = Math.Pow(z, 3) + 3 * Math.Pow(z, 2);
               }
             else if(z>=0&&z<=8)
               {
                x = Math.Sin(z);
               }
            else if(z>8)
               {
                x = Math.Exp(2) + Math.Exp(-z);
               }

            y = Math.Pow(x, 2) + 8 * x - 6;

            Console.WriteLine("Y = ");
            Console.WriteLine(y);
            return 0;
        }

        static void Main(string[] args)
        {
            int z = 0;
            double y = 0;
            double x = 0;
            Console.WriteLine("Enter z:");
            z = Convert.ToInt32(Console.ReadLine());
            math(z, x, y);
            Console.Read();

        }

    }
}
