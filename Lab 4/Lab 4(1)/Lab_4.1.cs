﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_4_1_
{
    class Program
    {
        static void Main(string[] args)
        {
            double x = 3;
            double summa = 0;
            while(x<5)
            {
                summa += (Math.Exp(-x / 3) + x) / (x + 1);
                x = x + 0.5;
            }

            Console.WriteLine("Summa = ");
            Console.WriteLine(summa);
            Console.Read();
        }
    }
}
